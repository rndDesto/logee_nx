import { AppProps } from 'next/app';
import Head from 'next/head';
import './styles.css';
import { Button } from '@logeeidpoc/mantul';

function CustomApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>Welcome to host-logee!</title>
      </Head>
      <main className="app">
        <Component {...pageProps} />
        <Button />
      </main>
    </>
  );
}

export default CustomApp;
